 
 WR4-Terminus - July 2010
 ------------------------
 by hilaire9
 -----------
 Length: 715 meters                                      
 Type: Extreme (WolfR4.exe enabled)
 Can be raced in Reverse mode.
 Has a hidden Practice Star.
 Plays a custom .mp3 file.
 Custom files folder: h9ter.
 =====================================================
 * To Install: Unzip directly into your main Re-volt folder.
 ======================================================
 Description: The final destination point of the
 transportation system in the City of Tomorrow.
 =======================================================
 Credit: 3D human face modified from anakinhead.3ds file
 from: http://www.3dmodelfree.com/models//26334-0.htm
 =======================================================
 Tools: 3D Studio Max 8, ASE Tools, ZModeler, MAKEITGOOD 
 edit modes and Paint Shop Pro 9.
 Total polygon count: 10,446.     
 Custom.ini system and WolfR4.exe created by jigebren.
 jigebren's webpage:  http://jigebren.free.fr/jeux/pc/revolt/WolfR4.html                 
------------------------------------------------------------------------
--- hilaire9's Home Page: http://members.cox.net/alanzia


 
