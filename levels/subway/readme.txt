Thanks for downloading Subway!

There's still room for improvement (especially AI) but hope you like the track!
I've got a concept for Subway2, but we'll see if I gather the motivation for this second episode.

If you're willing to improve the track in any way you are more than welcome to contact me on
discord / ReVolt World :)

Music : wire and Flashing Lights EP by Professor Kliq (https://www.professorkliq.com) 
Montazac ad by @AlexisVeille

Thanks Tubers for the train model,
Thanks Tubers and Hajduc for playtesting!
And thank you and to the whole RV community for keeping the game alive.

See ya,
Capitaine SZM