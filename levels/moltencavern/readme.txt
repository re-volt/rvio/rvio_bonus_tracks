Information
-----------------
Track Name:	Molten Caverns
Length:		1030 m (Normal) / 982 m (Reversed)
Difficulty		Medium
Author:		Saffron


Description
-----------------
My first ever track fully made in Blender. For the longest time I've wanted to make a track with a lava aesthetic, and thought now was the best time to do so.


Credits
-----------------
The Internet for the textures
The people who made Blender, and Marv for his extensive knowledge on it
Ali for RVMinis
Huki and Marv for RVGL
Rick Brewster for Paint.NET
Victor and Marv, among others for testing out of the track and giving feedback
Everyone in Re-Volt Discord