             _ _                       __           
  /\  /\__ _| | | _____      _____    /__\_   _____ 
 / /_/ / _` | | |/ _ \ \ /\ / / __|  /_\ \ \ / / _ \
/ __  / (_| | | | (_) \ V  V /\__ \ //__  \ V /  __/
\/ /_/ \__,_|_|_|\___/ \_/\_/ |___/ \__/   \_/ \___|
                                                    
----------------------------------------------------

Track Name:      Hallows Eve
Track Author:    zagames (Zach)
Track Length:    516 Meters
Difficulty:      Medium

----------------------------------------------------

Tools Used:      MSPaint
                 Adobe Photoshop CS
                 Autodesk 3DS Max
Time Invested:   20+ hours

----------------------------------------------------

Back Story:      Racing around abandoned buildings
                 and a cemetery. Midnight approaches
                 and weird things begin to happen...

----------------------------------------------------

Online:          http://rvzt.zackattackgames.com

----------------------------------------------------

Credits:         David Gurrea - For misc. textures
                 Hilaire9 - For all sorts of stuff
                 Nikki (my girlfriend) - For making
                     me finish the track
                 ASCII Generator - for the title
                 God - For countless blessings
