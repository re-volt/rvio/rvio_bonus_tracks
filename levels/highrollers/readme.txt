High Rollers by BloodBTF 2021-2022

An earthquake has ravaged the city of San Francisco. With all of the residents evacuated, this city belongs to us racers now. I've decided to head out to ground zero myself and setup an RC race circuit on one of the abandoned parking garages. With a track being this high up, there are bound to be disasters. I didn't bother to install any safety barriers on this track, because what would be the fun in that? I did however place many powerups throughout the many alternate paths for you to gain an advantage against your opponents. Find the best line for your vehicle to outpace your opponents, or use your rockets and balloons to send them falling to their demise below!

ASSETS USED:
skybox and building textures used from Sunset Hills by hilaire9
reverse skybox from toytanic 2
some track textures and BMW model from nhood1 by Acclaim
King Kaiju Drift prop by Trixed
"XENO" Billboard by Mace121

Tools used:
GIMP
Blender
RVGL

You may do whatever you want with this track and its assets, as long as you credit the approprate authors.

Thanks to the testers:
FZG


 