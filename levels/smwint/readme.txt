Hyperborean Harmony by Zeino (Tryxn on RVZ)

hi hello this is a cool and chilly circuit set in some cold country lmao. made specially for the 2021 Christmas Recollection event on IO. hope you have fun, enjoy the track and please oh PLEASE dont eat the ginger bread! haha  wishing you a happy christmas

credits:
the song is MerryXmass by TenodiBoris (https://www.newgrounds.com/audio/listen/989615) thank you Tubers for this great find!
textures: various sources, internet, Saffron's Dark Vale Forest, some edited by me
skybox taken from Marv's skybox pack
sounds  - cave ambience by rucisko on freesound.org
	- harmonic ambience sound Celestial atmosphere Do Major Harmonics (https://freesound.org/people/bolkmar/sounds/417498/) by bolkmar
	- birds ambience by 1ratatosk1 on freesound.org

Special thanks:
my friends At Home for keeping me going