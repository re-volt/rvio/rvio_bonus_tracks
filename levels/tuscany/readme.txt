
 Tuscany
 -----------------------------
 by hilarious6 --- May 2023
 ------------------
 Length: 508 meters                                      
 Type: Extreme
 ====================================================
 
 *** To Install: Unzip into your main Re-Volt folder.
 
 ====================================================
 * Description: Race around a typical Tuscany neighbourhood.
 ----------------------------------
 Textures from various sources, mainly from Urban Terror 4.1
 ----------------------------------
 * music - Occhi di Bambola by Giovanni Vicari https://freemusicarchive.org/music/Giovanni_Vicari/single/Occhi_di_Bambola/
 ------------------------------------------------------------
 --- Mi8xMCBhbHJlYWR5IGluIHRoZSBmaXJzdCB3ZWVrLCBpIHdvbmRlciBob3cgbG9uZyBpIGNhbiBrZWVwIHVwPw==
 
 
