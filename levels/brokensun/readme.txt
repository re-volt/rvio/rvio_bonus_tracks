.=+=+=+=+=+=+=+=.
+ General Info  +
*=+=+=+=+=+=+=+=*

	Track Name: 			Broken Sunlight
	จจจจจจจจจจ                         
	Folder Name:			brokensun
	จจจจจจจจจจจจ 
	Authors:			R6TurboExtreme and Kfalcon (aka KDL)
	จจจจจจจ				
	Track Type:			Extreme 
	จจจจจจจจจจจ
	Track Length:			920m
	จจจจจจจจจจจจจ
	Difficulty:			Just like eating pepper after Harissa (Hot chili sauce).
	จจจจจจจจจจ


	Tools used:			* Modeling: gMax, 3Ds max 
	จจจจจจจจจจจ			  จจจจจจจจ
					* Textures: Paint.NET
					  จจจจจจจจ
					* Exporting: Ase2w (July'30th 2012) 
					  จจจจจจจจจ
					* Animation: TexYUI
					  จจจจจจจจจจ
					* Clean/Pack: Rv Organizer
					  จจจจจจจจจจ



.=+=+=+=+=+=+=+=.
+ Description   +
*=+=+=+=+=+=+=+=*
 Away Far away from NewVolt city, exists a lost village called "bro ken-san lit" which nobody visit nomore.
 At the broken Sunlight, "bro ken-san lit" race was held, from mayan statues to latin mines and middle age tools.


.=+=+=+=+=+=+=+=.
+ Re-Volt 1.2a  +
*=+=+=+=+=+=+=+=*
 Re-Volt 1.0, 1.1 (Patch 09.16 and 12.07) are obsolete.
 Consider upgrading to the latest Re-Volt 1.2 alpha.



.=+=+=+=+=+=+=+=.
+   Frame Rate  +
*=+=+=+=+=+=+=+=*
 Sadly, we've optimized the track many times, converted some 32-bit bitmaps to 24 ones, done visiboxes,
  yet,  the frame rate is still low compared to other tracks.
   Well, it's possible to lower resolution or access .inf file and lower FOGSTART and FARCLIP to get good results.



.=+=+=+=+=+=+=+=.
+     WORK      +
*=+=+=+=+=+=+=+=*

Modeling:

			* Main track modeler: R6TurboExtreme
		 	 จจจจจจจจจจจจจจจจจจจจ
			* Instances, UV fix and shaders: Kfalcon
			 จจจจจจจจจจจจจจจจจจจจจจจจจจจจจจ

MakeItGood:
			* POS-AI-TAZ: R6TurboExtreme
			  จจจจจจจจจจจ
			* Triggers: R6TurboExtreme & Kfalcon
			  จจจจจจจจ
			* Lights-object-cams: Kfalcon
			  จจจจจจจจจจจจจจจจจจ
			* Viz: Kfalcon
			  จจจ


Textures are from the internet (two are borrowed from Country Club)
Skybox converted from redsorceress

The old ASE2W keeps aborting the export so We had to modify the sources to make 'If Length = 0' Do Warning 
instead of Error + fixed morph's SG 1 and 2 to become 'Normal1', 'Normal2' instead of 'Windy1','Windy2'.
 (Visual C++ 2008 runtime is required in case you're interested)


.=+=+=+=+=+=+=+=.
+  Thanks 	+
*=+=+=+=+=+=+=+=*

 You for downloading
 Acclaim (Probe and Iguana) for this nice game!
 Jigebren and Huki for Re-Volt 1.2 and Jigebren for World Cut
 Ali and Gabor for ASE2W
 ADX for converting ase2RV to CMAKE
 Dave for teaching me about the Viziboxes
 Re-Volt Live team for support (Balint, Dave, ZR, miromiro...)
   Special thanks to Balint and miromiro for suggestions and ideas.
   Special thanks to Dave-o-rama, StrixMidnight, MythicMonkey, Skarma
		      mmudshark and Jackieben for their support. We really mean it! :D



.=+=+=+=+=+=+=+=.
+  Support etc  +
*=+=+=+=+=+=+=+=*

 * Re-Volt Live: http://z3.invisionfree.com/Revolt_Live

 
.=+=+=+=+=+=+=+=.
+  Copyrights   +
*=+=+=+=+=+=+=+=*
  You're free to modify/take some/remove some parts of this track But world ISNOT free. So, please one of these ;) :
   (1) Include this readme as it is on your project (don't worry, people won't read it!)
   (2) mention it on your readme!
 