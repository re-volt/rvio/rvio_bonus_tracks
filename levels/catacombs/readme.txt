================================================================
            Catacombs by scloink and triple6s
================================================================

Track: The Catacombs

Author/s: scloink and triple6s

Length: 729 meters

Folder Name: Catacombs

Install: Unzip with file names on to the main ReVolt folder

Tools used: PSP 7.0, 3D Studio Max 3.1, The ReVolt track editor, 
            ase converters by ali and Antimorph and Glue4 by ali.

-----------------------------------------------------------------
UPDATE(sjampo): The track has now been converted to WolfR4 format.

To enjoy the custom elements of this track, just download the
program WolfR4 from the link below. No batch files are needed.

http://jigebren.free.fr/jeux/pc/revolt/WolfR4.html
-----------------------------------------------------------------

=================================================================
                        Options
=================================================================

***Note:***
The install_Catacombs_custom and uninstall_Catacombs_custom
batch files must be located withing your main revolt folder to 
work!!
 
This track has two options to running it. With animated axes and 
without. There are install and uninstall batch files located 
within this zip file. For the full effect I would recommend using
the animated axes. It just adds to the track. If you choose to use
them the rocking horse will be replaced by running the install 
batch file. Don't worry because the original will be backed up  
and is replaceable by running the uninstall batch file. Default is
without animation.

***Note***
To use the animated axes you must click on the install_Catacombs_Custom
batch file. If you click on it and there are still no axes then the 
model that is being replaced is a read only. To change this you need to 
go into the models folder and select the horse.m then right click.
Then change the attributes from read only to archive. Once you have 
done this just click the install batch again. 


================================================================
                     Construction
================================================================
The main idea of this track was to see how far we could take a 
lego track without making it extreme. There are several areas 
in this track which for all intents and purposes are completely
extreme. It has made many transformations over the months which
I think were totally for the better. The layout was meant to be 
challenging so don't expect to take your speed demon car into
this track and come out victorious unless you know how to slow
down. 


================================================================
                  Thanks and Accolades
================================================================

First off I would like to thank the entire community for hanging
around through all that has happened. I think we can compare 
ourselves to some of those Quake clans out there. We are the true 
fanatics of Re-Volt. 

I would also like to thank Wayne Lemonds for the great forum and 
website. He has helped bring some people together as aquaintances
as well as some friends. If you havn't visited his website please
do so, you won't regret it. 
www.racerspoint.com


Now for the individual thanks,

triple6s (Jeff):
Without him to help me with this track it may have never made it 
out. He has help more than he knows or cares to admit.

SuperTard:
He has been there with me from the begining encouraging me on with 
this project. He also made the great ghost you will see in the 
lower dungeon room.

IronBob:
He has helped with beta testing as well as making the axe model.

Antimorph:
Without his help in max and his suggestions during testing this
track would never had turned into what it is.

Skitch:
He too has helped so much in max and with textures as well. Skitch 
made the skeleton texture you will see in the jail cells in the 
lower dungeon. He also contributed in the testing.
 
Silverlode:
He has helped in testing and with suggestions on how to improve
the overall feel and look.

CADster:
Testing and suggestion on improvement.

Graham:
Also Testing and improvement

If I have left anyone out I am sorry and feel free to reprimand me 
for it on the forum. 


I would also like to thank my better half for not divorcing me
while I pursued the completion of this project. She has been great
even though she hates this darn game.

================================================================