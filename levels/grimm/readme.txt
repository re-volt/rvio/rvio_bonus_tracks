=====================================================================
Nhood Grimm by Skitch with help from the community
=====================================================================
Track Name              : Nhood Grimm
 
Install in folder       : Unzip to your main ReVolt folder and all files will go where needed

Author/s                : Skitch with help from scloink, supertard, DSL_Tile
====================================================================
Description:
A fairly dark track with a very fast layout. Has a few technical sections. Keep your eyes open because if you don't you will loose. I added as many powerups as I could to make it fun in multiplayer. 

***************************************************************************************************
NOTE: THIS INCLUDES CUSTOM MODELS WHICH WILL AUTOMACIALLY REPLACE THE RINGS AND THE HORSE. IT IS 
RECOMMENDED THAT YOU USE THESE MODELS WHICH ADD SOME NEAT LITTLE EFFECTS TO THE TRACK.
***************************************************************************************************

Creation:
The mesh was created and textured in Max 3.1 by Skitch with the exception of the dumpsters and the boxes in the middle of the road. Those were added by scloink. SuperTard did the cameras for the track which give some great replays as well as the Visiboxes. DSL_Tile helped to remove all the annoying X's that kept popping up and scloink did the AI, POS, Zones, Triggers, Lights, object placement as well as adding a few more ligt poles in max.  


Tools Used:
3Ds Max 3.1
Photoshop

Thanks:
Thanks go out to the entire community and especially to Wayne Lemonds because without his support during GA Games romp through our lovely community the RV scene would have probably died because his message board helped keep people around and interested.

Special Thanks:
scloink - Most of the Extreme Editing part of the track as well as some Max work.
SuperTard - Doing the Cameras and Visiboxes.
triple6s - Testing
DSL_Tile - Help with finding X's and tweaking the Zones and nodes to fix them.
SlowJustice - Testing
 
 

Visit:
www.rvarchive.com - The Revolt Archive
www.racerspoint.com - The Racers Point, home of the greatest forum anywhere


