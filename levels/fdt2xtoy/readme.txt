Track information
================================================================
Track name :	Toy World EX
Track Type :	Extreme
Length :		573
Difficulty:	Extreme

Author Information
================================================================
Author Name :	Xarc
Email Address : 	warrockrockwar@hotmail.it

Construction
================================================================
Editor(s) used : text/image editors and tool's/utilities used

Blender
MediBang Paint Pro

Additional Credits 
================================================================
Thanks and Credits to those who inspired or helped

jigebren for the Blender plugin

Copyright / Permissions
================================================================
You may do whatever you want with this Track. Provided you include this file, with NO modifications. 