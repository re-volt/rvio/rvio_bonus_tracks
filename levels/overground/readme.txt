Information
-----------------
Track Name:	Overground
Length:		694 m
Type:		Extreme
Author:		Saffron


Description
-----------------
It's been a while. Six years since I last released a proper track.

The base was made in ZModeler, modified a bit in Blender, spiced up with PRM kits. The raceline is relatively straightforward, with plenty of obstacles that won't necessarily be a nuisance or get in your way, but their presence will keep you on your toes.


Credits
-----------------
The Internet for the textures
Oleg for ZModeler
The people who made Blender
Ali for RVMinis and RVGlue
Huki for RVGL
Rick Brewster for Paint.NET
Victor, Skarma and Marv for testing out of the track and giving feedback
Everyone in Re-Volt Discord