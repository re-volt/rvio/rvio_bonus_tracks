	MATERIAL {
	  ID              22
	  Name            "ICE 3"
	  Grip            0.9
	}

	SPARK {
	  ID              31
	  Name            "FUNNY"
	  CollideWorld    false
	  CollideObject   false
	  CollideCam      false
	  HasTrail        false
	  FieldAffect     false
	  Spins           true
	  Grows           true
	  Additive        true
	  Horizontal      false
	  Size            1 1
  	UV              0 0
  	UVSize          0.25 0.25
  	TexturePage     47
	  Color           25 25 25
	  Mass            0.05
	  Resistance      0.002
	  Friction        0
	  Restitution     0
	  LifeTime        30.3
	  LifeTimeVar     0
	  SpinRate        0
	  SpinRateVar     0
	  SizeVar         2
	  GrowRate        0
	  GrowRateVar     3
	}

PICKUPS {
  SpawnCount      64 64                           ; Initial and per-player count
  EnvColor        255 255 128                   ; Color of shininess (RGB)
  LightColor      10 10 10                      ; Color of light (RGB)
}