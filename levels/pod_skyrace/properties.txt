{
  PICKUPS {
    EnvColor       255 255 255
    LightColor     102 255 0
  }

MATERIAL {
  ID              1                             ; Material to replace [0 - 26]
  Name            "MARBLE"                     ; Display name

  Skid            false                          ; Skidmarks appear on material
  Spark           false                          ; Material emits particles
  OutOfBounds     false                         ; Not implemented
  Corrugated      false                         ; Material is bumpy
  Moves           false                         ; Moves like museum conveyors
  Dusty           false                         ; Material emits dust

  Roughness       0.000000                      ; Roughness of the material
  Grip            2.000000                      ; Grip of the material
  Hardness        1.000000                      ; Hardness of the material

  SkidSound       0                             ; Sound when skidding
  ScrapeSound     0                             ; Car body scrape [5:Normal]

  SkidColor       112 112 112                   ; Color of the skidmarks
  CorrugationType 0                             ; Type of bumpiness [0 - 7]
  DustType        0                             ; Type of dust
  Velocity        0.000000 0.000000 0.000000    ; Move cars
}

}