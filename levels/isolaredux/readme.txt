Isola Verde Redux by MightyCumber and Gotolei
Original by Crescviper

General Information
---------------------------
Name: Isola Verde Redux
Authors: MightyCucumber, Gotolei
Type: Extreme
Length: 740m

Description
-------------------
Redux of the track Isola Verde by Crescviper (see readme-original.txt).
Improved graphics all around, as well as a few small fixes for smoother gameplay.
Mightycucumber: All-new textures, additional objects
Gotolei: Lighting, remodeling, mapping fixes

Programs Used
---------------------
Blender (Marv's 2.79 Re-Volt plugin)
Photoshop and Gimp

v. 2022-09-16

