

-------------------------------------------------------------------Isola verde--------------------------------------------------------------------------

----------------------------------------------------------------------by Crescviper----------------------------------------------------------------------------------



General nformation:
------------

Name:          Isola verde

Author:         Crescviper

Type:            Extreme

Length:         740 m.

Difficult:        8/10 (with stock car)

Surface:       The surface installed by me with ASE TOOLS are: The Default Surface,the grass surface and trick surface (in -morph mode)

-------------
Installation:
-------------

You must unzip the Isola verde.zip into your main Re-Volt directory.

------------
Descrizione (italiano):
------------

Questa � la mia terza pista fatta con gli ASE TOOLS e 3dsmax

La pista � ambientata un parco acquatico realmente esistente che si trova a Pontecagnano (sa) in Italia

dove sono andato personalmente con amici, e mi � piaciuto cos� tanto da riuscire a farci una pista per re-volt.

------------------------------------------------------------------------------------------------------------------
------------

Description (English):
------------

This is my third track that i made with ASETOOLS and 3dsmax

The track is situated in an acquatic park actually existing in Pontecagnano (sa) in Italy

where I went in person with friends, and I liked it so much to be able to give us a run for re-volt.

----------------------------------------

Programs used by me for make this track:

----------------------------------------

3dsmax
ASE TOOLS
Makeitgood
Paint of windows XP
Wolfr4 by Jigebren (to enable the skymap and the other gfx)

---------------------------------------

Special thanks to the italian site "Aliasrevoltmaster" and the english site "RVZT"


Copyright by Crescviper 2011...All right reserved
